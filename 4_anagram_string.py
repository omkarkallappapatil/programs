def anagram(str1, str2):
	if len(str1) != len(str2):
		return False
	if sorted(str1) != sorted(str2):
		return False
	else:
		return True

print(anagram("ball", "bat"))